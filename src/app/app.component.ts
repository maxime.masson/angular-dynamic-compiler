import { Component, ViewContainerRef, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { DynamicCompileService } from './services/dynamic-compile.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, AfterViewInit {
  name = 'from Angular';
  apiResult: string;

  @ViewChild('dynamic', { read: ViewContainerRef }) viewContainerRef: ViewContainerRef;

  constructor(public dynamicCompileService: DynamicCompileService) { }

  ngOnInit(): void {
    this.apiResult = `<h3>Im generated on the fly</h3> <app-example>Test</app-example>`;
  }
  ngAfterViewInit(): void {
    this.dynamicCompileService.createComponent(this.apiResult, this.viewContainerRef);
  }
}
